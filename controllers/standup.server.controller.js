/**
 * Created by Jonathan on 17/02/2015.
 */

var Standup = require('../models/standup.server.model.js');

exports.getNote = function(req, res) {
    res.render('newnote', { title: 'Standup - New Note'});
};

exports.create = function(req, res) {
    var entry = new Standup ({
        memberName: req.body.memberName,
        project: req.body.project,
        workYesterday: req.body.workYesterday,
        workToday: req.body.workToday,
        impediment: req.body.impediment
    });

    entry.save(); // handle error conditions in passed in function

    // redirect to home page with 301 code ()...
    res.redirect(301, '/');
};