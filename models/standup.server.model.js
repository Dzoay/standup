/**
 * Created by Jonathan on 17/02/2015.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var standupSchema = new Schema({
   memberName: String,
    project: String,
    workYesterday: String,
    workToday: String,
    impediment: String,
    createdOn: { type: Date, default: Date.now }
});

// export model
module.exports = mongoose.model('Standup', standupSchema);